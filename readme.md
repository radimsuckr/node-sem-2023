# node-sem

A simple implementation of a real-time chatting application built with Node.js and Express.

## Features

To be able to chat, the users must create an account and log in. After that, they can either join an already existing room or create a new one.

The "room view" is simply a list of messages sorted with the most recent on the top. There's also a message field to send a new message.

## Running

1. You need Node.js installed with the NPM (Node Package Manager)
2. Change working directory to the project
3. Run `npm install`
4. Optionally run `npm run test`
5. Run `npm run start`

The application should then launch on `http://localhost:3000`.

## Implementation

The real-time version works thanks to SSE (Server-Sent Events) and asynchronous server calls.

When a user opens a room, the browser subscribes to SSE endpoint on the server. This endpoint then pushes any new messages from the current room into user's browser and get's new messages as they come. Sending of new messages is then handled by using the Fetch API to asynchronously call endpoint `POST /room/:id/post-message`. Thanks to that the app is real-time and does not reload the whole page.

The endpoint for sending messages consumes either JSON documents for asynchronous calls or form data for plain form submissions. Because of this the app works even with disabled JavaScript but then it does not get any of the real-time features and depends on full page reloads. It also displays a warning that the features are limited without JavaScript.

### Why not WebSocket?

With SSE we don't have to invent our own messaging protocol. Also we can leverage existing support for HTTP in browsers and servers.

Even though chat applications need very fast message delivery, users consume messages way more than they send new ones. Thus bidirectional channel does not make that much sense and we're better off with simple server-pushed events and asynchronous callback.

### Other details

Other than that the app is fairly simple. It uses `knex` for database access and `ava` with `supertest` for some very simple tests.
