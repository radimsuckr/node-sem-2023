/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
export const up = async (knex) => {
  await knex.schema.createTable('room', (table) => {
    table.increments('id');
    table.string('name').notNullable();
    table.integer('author').index().references('id').inTable('user').onDelete('set null');
  });

  await knex.schema.createTable('message', (table) => {
    table.increments('id');
    table.string('content').notNullable();
    table.integer('author').index().references('id').inTable('user').onDelete('set null');
    table.integer('room').index().references('id').inTable('room').onDelete('cascade');
    table.timestamp('createdAt').defaultTo(knex.fn.now());
  });
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
export const down = async (knex) => {
  await knex.schema.dropTable('message');
  await knex.schema.dropTable('room');
};
