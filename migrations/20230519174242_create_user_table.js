/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
export const up = async (knex) => {
  await knex.schema.createTable('user', (table) => {
    table.increments('id');
    table.string('username').notNullable().unique();
    table.string('salt').notNullable();
    table.string('hash').notNullable();
    table.string('token').notNullable();
  });
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
export const down = async (knex) => {
  await knex.schema.dropTable('user');
};
