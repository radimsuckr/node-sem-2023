import { db } from '../database.js';

export const getRooms = async () => {
  return await db('room').select('*');
};

export const getRoomById = async (id) => {
  return await db('room').select('*').where({ id }).first();
};

export const deleteRoom = async (id) => {
  await db('room').where({ id }).delete();
};

export const createRoom = async (name, author) => {
  return await db('room').insert({ name, author }).returning(['id']);
};
