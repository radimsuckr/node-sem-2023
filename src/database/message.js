import { db } from '../database.js';

export const getMessagesByRoomId = async (room) => {
  return await db('message').select('message.*', 'user.*').join('user', 'user.id', 'message.author').where({ room }).orderBy('createdAt', 'desc');
};

export const getMessageById = async (id) => {
  console.log(id);
  return await db('message').select('message.content', 'message.createdAt', 'user.username').join('user', 'user.id', 'message.author').where({ 'message.id': id }).first();
};

export const createMessage = async (author, content, room) => {
  const createdAt = db.fn.now();
  return await db('message').insert({ author, content, createdAt, room }).returning('id');
};
