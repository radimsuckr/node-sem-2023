import crypto from 'node:crypto';
import { db } from '../database.js';

const ITERATIONS = 100000;

export const createUser = async (username, password) => {
  const salt = crypto.randomBytes(16).toString('hex');
  const hash = crypto.pbkdf2Sync(password, salt, ITERATIONS, 64, 'sha512').toString('hex');
  const token = crypto.randomBytes(16).toString('hex');

  const [user] = await db('user').insert({ username, salt, hash, token }).returning('*');

  return user;
};

export const getUserByPassword = async (username, password) => {
  const user = await db('user').where({ username }).first();
  if (!user) {
    return null;
  }

  const hash = crypto.pbkdf2Sync(password, user.salt, ITERATIONS, 64, 'sha512').toString('hex');
  if (hash !== user.hash) {
    return null;
  }

  return user;
};

export const getUserByToken = async (token) => {
  const user = await db('user').where({ token }).first();

  return user;
};
