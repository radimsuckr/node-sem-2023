import express from 'express';
import { createUser, getUserByPassword } from './database/user.js';
import loadUser from './middleware/loadUser.js';
import cookieParser from 'cookie-parser';
import { createRoom, deleteRoom, getRoomById, getRooms } from './database/room.js';
import { createMessage, getMessageById, getMessagesByRoomId } from './database/message.js';

export const app = express();

app.set('view engine', 'ejs');

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(loadUser);

const clients = new Map();

app.get('/', async (_req, res) => {
  const rooms = await getRooms();

  return res.render('index', {
    rooms,
  });
});

app.get('/register', (_req, res) => {
  return res.render('register');
});

app.post('/register', async (req, res) => {
  const user = await createUser(req.body.username, req.body.password);
  res.cookie('token', user.token);
  return res.redirect('/');
});

app.get('/login', (_req, res) => {
  return res.render('login');
});

app.post('/login', async (req, res) => {
  const user = await getUserByPassword(req.body.username, req.body.password);
  if (!user) {
    return res.status(400).render('400');
  }
  res.cookie('token', user.token);
  return res.redirect('/');
});

app.get('/logout', (_req, res) => {
  res.cookie('token', '');
  return res.redirect('/');
});

app.post('/create-room', async (req, res) => {
  await createRoom(req.body.name, res.locals.user.id);
  return res.redirect('/');
});

app.get('/delete-room/:id', async (req, res) => {
  const roomId = req.params.id;
  await deleteRoom(roomId);
  return res.redirect('/');
});

app.get('/room/:id', async (req, res) => {
  const roomId = req.params.id;
  const room = await getRoomById(roomId);
  if (!room) {
    return res.status(404).render('404');
  }

  const messages = await getMessagesByRoomId(room.id);

  return res.render('room', {
    messages,
    room,
  });
});

app.post('/room/:id/post-message', async (req, res) => {
  const roomId = req.params.id;
  const room = await getRoomById(roomId);
  if (!room) {
    return res.status(404).render('404');
  }

  const contentType = req.headers['content-type'];
  if (contentType !== 'application/json' && contentType !== 'application/x-www-form-urlencoded') {
    return res.status(400).render('400');
  }

  const content = contentType === 'application/json' ? req.body.content : req.body['content'];
  const messageId = await createMessage(res.locals.user.id, content, room.id);
  const message = await getMessageById(messageId[0]['id']);
  const roomClients = clients.get(room.id);
  if (roomClients) {
    for (const client of roomClients) {
      client.response.write('event: message\n');
      client.response.write(`data: ${JSON.stringify(message)}\n\n`);
    }
  }

  if (contentType === 'application/json') {
    return res.status(201);
  } else {
    return res.redirect(`/room/${room.id}`);
  }
});

app.get('/room/:id/messages', async (req, res) => {
  const roomId = req.params.id;
  const room = await getRoomById(roomId);
  if (!room) {
    res.setHeader('Content-Type', 'application/json');
    return res.end(JSON.stringify(`Room "${roomId}"`));
  }

  const headers = {
    'Cache-Control': 'no-cache',
    'Connection': 'keep-alive',
    'Content-Type': 'text/event-stream',
  };
  res.writeHead(200, headers);

  const clientId = res.locals.user.id;
  const newClient = {
    id: clientId,
    response: res,
  };

  let roomClients = clients.get(room.id);
  if (!roomClients) {
    roomClients = new Set();
  }
  roomClients.add(newClient);
  clients.set(room.id, roomClients);

  req.on('close', () => {
    console.log(`Client ${clientId} closed connection`);
    roomClients = clients.get(room.id);
    roomClients.delete(newClient);
  });
});

app.use((_req, res) => {
  res.status(404).render('404');
});

export default app;
