import test from 'ava';
import { createRoom, deleteRoom, getRoomById, getRooms } from '../src/database/room.js';
import { createUser } from '../src/database/user.js';
import { db } from '../src/database.js';

test.beforeEach(async () => {
  await db.migrate.latest();
});

test.afterEach.always(async () => {
  await db.migrate.rollback();
});

test.serial('create room', async t => {
  const user = await createUser('test', 'test');
  const roomId = (await createRoom('room', user.id))[0];
  const room = await getRoomById(roomId.id);
  t.is(room.name, 'room');
});

test.serial('delete room', async t => {
  const user = await createUser('test', 'test');
  const room = (await createRoom('room', user.id))[0];
  await deleteRoom(room.id);
  const rooms = await getRooms();
  t.is(rooms.length, 0);
});

test.serial('get rooms', async t => {
  const user = await createUser('test', 'test');
  const room1 = (await createRoom('room1', user.id))[0];
  const room2 = (await createRoom('room2', user.id))[0];
  const room3 = (await createRoom('room3', user.id))[0];
  t.deepEqual((await getRooms()).map(x => { return { id: x.id }; }), [room1, room2, room3]);
});

test.serial('get rooms by ids', async t => {
  const user = await createUser('test', 'test');
  const room1 = (await createRoom('room1', user.id))[0];
  const room2 = (await createRoom('room2', user.id))[0];
  const room3 = (await createRoom('room3', user.id))[0];
  t.is((await getRoomById(room1.id)).id, room1.id);
  t.is((await getRoomById(room2.id)).id, room2.id);
  t.is((await getRoomById(room3.id)).id, room3.id);
});
