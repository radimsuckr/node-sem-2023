import app from '../src/app.js';
import request from 'supertest';
import test from 'ava';
import { createUser } from '../src/database/user.js';
import { db } from '../src/database.js';

test.before(async () => {
  await db.migrate.latest();
});

test.after.always(async () => {
  await db.migrate.rollback();
});

test('POST /login', async (t) => {
  await createUser('tobi', 'tobi');

  const res = await request(app)
    .post('/login')
    .send({ username: 'tobi', password: 'tobi' })
    .set('Accept', 'text/html')
    .expect('Content-Type', /html/)
    .expect(302);

  t.is(res.status, 302);
});
