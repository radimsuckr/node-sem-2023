export default {
  development: {
    client: 'sqlite3',
    connection: {
      filename: './app-dev.db',
    },
    useNullAsDefault: false,
  },
  test: {
    client: 'sqlite3',
    connection: {
      filename: ':memory:',
    },
    useNullAsDefault: false,
  },
};
